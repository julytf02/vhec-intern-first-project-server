﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using vhec_intern_first_project_server.Models;

namespace vhec_intern_first_project_server.Data;

public class DataSeeder
{
    static private AppDbContext _appDbContext;
    static private UserManager<User> _userManager;

    public static async Task Seed(IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();

        _appDbContext = scope.ServiceProvider.GetService<AppDbContext>();
        _userManager = scope.ServiceProvider.GetService<UserManager<User>>();

        await SeedUsers();

        await _appDbContext.SaveChangesAsync();
    }

    static async Task SeedUsers()
    {
        var users = new[] {
            new {
                Name = "Lâm Chấn Vũ",
                Email = "vulam@gmail.com",
                Password = "password",
                PhoneNumber = "0901234561",
                Address = "123 Ho Chi Minh City"
            },
            new {
                Name = "Nguyễn Thị Hương",
                Email = "nguyenthihuong@example.com",
                Password = "password2",
                PhoneNumber = "0901234562",
                Address = "456 Hanoi"
            },
            new {
                Name = "Lê Minh Tâm",
                Email = "leminhtam@example.com",
                Password = "password3",
                PhoneNumber = "0901234563",
                Address = "789 Da Nang"
            },
            new {
                Name = "Phạm Hoàng Nam",
                Email = "phamhoangnam@example.com",
                Password = "password4",
                PhoneNumber = "0901234564",
                Address = "101 Can Tho"
            },
            new {
                Name = "Hoàng Thị Ngọc",
                Email = "hoangthingoc@example.com",
                Password = "password5",
                PhoneNumber = "0901234565",
                Address = "102 Hue"
            },
            new {
                Name = "Vũ Văn Long",
                Email = "vuvanlong@example.com",
                Password = "password6",
                PhoneNumber = "0901234566",
                Address = "103 Vung Tau"
            },
            new {
                Name = "Bùi Thị Mai",
                Email = "buithimai@example.com",
                Password = "password7",
                PhoneNumber = "0901234567",
                Address = "104 Nha Trang"
            },
            new {
                Name = "Đỗ Hồng Hạnh",
                Email = "dohonghanh@example.com",
                Password = "password8",
                PhoneNumber = "0901234568",
                Address = "105 Da Lat"
            },
            new {
                Name = "Dương Văn Hùng",
                Email = "duongvanhung@example.com",
                Password = "password9",
                PhoneNumber = "0901234569",
                Address = "106 Quy Nhon"
            },
            new {
                Name = "Trần Thị Thu",
                Email = "tranthithu@example.com",
                Password = "password10",
                PhoneNumber = "0901234570",
                Address = "107 Hai Phong"
            },
            new {
                Name = "Phan Minh Tuấn",
                Email = "phanminhtuan@example.com",
                Password = "password11",
                PhoneNumber = "0901234571",
                Address = "108 Thai Binh"
            },
            new {
                Name = "Lương Thị Hằng",
                Email = "luongthihang@example.com",
                Password = "password12",
                PhoneNumber = "0901234572",
                Address = "109 Nam Dinh"
            },
            new {
                Name = "Nguyễn Văn Khánh",
                Email = "nguyenvankhanh@example.com",
                Password = "password13",
                PhoneNumber = "0901234573",
                Address = "110 Bac Ninh"
            },
            new {
                Name = "Vũ Thị Bích",
                Email = "vuthibich@example.com",
                Password = "password14",
                PhoneNumber = "0901234574",
                Address = "111 Lang Son"
            },
            new {
                Name = "Bùi Văn Hòa",
                Email = "buivanhoa@example.com",
                Password = "password15",
                PhoneNumber = "0901234575",
                Address = "112 Lao Cai"
            },
            new {
                Name = "Trần Thị Thanh",
                Email = "tranthithanh@example.com",
                Password = "password16",
                PhoneNumber = "0901234576",
                Address = "113 Dien Bien"
            },
            new {
                Name = "Nguyễn Văn Dũng",
                Email = "nguyenvandung@example.com",
                Password = "password17",
                PhoneNumber = "0901234577",
                Address = "114 Hoa Binh"
            },
            new {
                Name = "Lê Thị Lan",
                Email = "lethilan@example.com",
                Password = "password18",
                PhoneNumber = "0901234578",
                Address = "115 Hai Duong"
            },
            new {
                Name = "Phạm Văn Hải",
                Email = "phamvanhai@example.com",
                Password = "password19",
                PhoneNumber = "0901234579",
                Address = "116 Hung Yen"
            },
            new {
                Name = "Hoàng Thị Kim",
                Email = "hoangthikim@example.com",
                Password = "password20",
                PhoneNumber = "0901234580",
                Address = "117 Ha Tinh"
            },
            new {
                Name = "Vũ Văn Quân",
                Email = "vuvanquan@example.com",
                Password = "password21",
                PhoneNumber = "0901234581",
                Address = "118 Quang Binh"
            },
            new {
                Name = "Bùi Thị Hồng",
                Email = "buithihong@example.com",
                Password = "password22",
                PhoneNumber = "0901234582",
                Address = "119 Quang Tri"
            },
            new {
                Name = "Nguyễn Văn Tùng",
                Email = "nguyenvantung@example.com",
                Password = "password23",
                PhoneNumber = "0901234583",
                Address = "120 Thua Thien Hue"
            },
            new {
                Name = "Lê Thị Thảo",
                Email = "lethithao@example.com",
                Password = "password24",
                PhoneNumber = "0901234584",
                Address = "121 Quang Nam"
            },
            new {
                Name = "Phan Văn Minh",
                Email = "phanvanminh@example.com",
                Password = "password25",
                PhoneNumber = "0901234585",
                Address = "122 Quang Ngai"
            },
            new {
                Name = "Hoàng Thị Ngân",
                Email = "hoangthingan@example.com",
                Password = "password26",
                PhoneNumber = "0901234586",
                Address = "123 Binh Dinh"
            },
            new {
                Name = "Đặng Văn Hùng",
                Email = "dangvanhung@example.com",
                Password = "password27",
                PhoneNumber = "0901234587",
                Address = "124 Phu Yen"
            },
            new {
                Name = "Trần Thị Thuỳ",
                Email = "tranthithuy@example.com",
                Password = "password28",
                PhoneNumber = "0901234588",
                Address = "125 Khanh Hoa"
            },
            new {
                Name = "Phạm Văn Hòa",
                Email = "phamvanhoa@example.com",
                Password = "password29",
                PhoneNumber = "0901234589",
                Address = "126 Ninh Thuan"
            },
            new {
                Name = "Nguyễn Thị Mỹ",
                Email = "nguyenthimy@example.com",
                Password = "password30",
                PhoneNumber = "0901234590",
                Address = "127 Binh Thuan"
            },
            new {
                Name = "Lê Văn Dương",
                Email = "levanduong@example.com",
                Password = "password31",
                PhoneNumber = "0901234591",
                Address = "128 Kon Tum"
            },
            new {
                Name = "Vũ Thị Hồng",
                Email = "vuthihong@example.com",
                Password = "password32",
                PhoneNumber = "0901234592",
                Address = "129 Gia Lai"
            },
            new {
                Name = "Bùi Văn Phú",
                Email = "buivanphu@example.com",
                Password = "password33",
                PhoneNumber = "0901234593",
                Address = "130 Dak Lak"
            },
            new {
                Name = "Nguyễn Thị Phương",
                Email = "nguyenthiphuong@example.com",
                Password = "password34",
                PhoneNumber = "0901234594",
                Address = "131 Dak Nong"
            },
            new {
                Name = "Lê Thị Ngọc",
                Email = "lethingoc@example.com",
                Password = "password35",
                PhoneNumber = "0901234595",
                Address = "132 Lam Dong"
            },
            new {
                Name = "Trần Văn Hưng",
                Email = "tranvanhung@example.com",
                Password = "password36",
                PhoneNumber = "0901234596",
                Address = "133 Binh Phuoc"
            },
            new {
                Name = "Phan Thị Thủy",
                Email = "phanthithuy@example.com",
                Password = "password37",
                PhoneNumber = "0901234597",
                Address = "134 Tay Ninh"
            },
            new {
                Name = "Đinh Văn Hiếu",
                Email = "dinhvanhieu@example.com",
                Password = "password38",
                PhoneNumber = "0901234598",
                Address = "135 Binh Duong"
            },
            new {
                Name = "Trần Thị Kim",
                Email = "tranthikim@example.com",
                Password = "password39",
                PhoneNumber = "0901234599",
                Address = "136 Long An"
            },
            new {
                Name = "Nguyễn Văn Tiến",
                Email = "nguyenvantien@example.com",
                Password = "password40",
                PhoneNumber = "0901234600",
                Address = "137 Đồng Tháp"
            },
            new {
                Name = "Hoàng Thị Hạnh",
                Email = "hoangthihanh@example.com",
                Password = "password41",
                PhoneNumber = "0901234601",
                Address = "138 An Giang"
            },
            new {
                Name = "Vũ Văn Hải",
                Email = "vuvanhai@example.com",
                Password = "password42",
                PhoneNumber = "0901234602",
                Address = "139 Tiền Giang"
            },
            new {
                Name = "Trần Thị Hằng",
                Email = "tranthihang@example.com",
                Password = "password43",
                PhoneNumber = "0901234603",
                Address = "140 Kiên Giang"
            },
            new {
                Name = "Đặng Văn Thành",
                Email = "dangvanthanh@example.com",
                Password = "password44",
                PhoneNumber = "0901234604",
                Address = "141 Cần Thơ"
            },
            new {
                Name = "Phạm Thị Thúy",
                Email = "phamthithuy@example.com",
                Password = "password45",
                PhoneNumber = "0901234605",
                Address = "142 Hậu Giang"
            },
            new {
                Name = "Hoàng Văn Hùng",
                Email = "hoangvanhung@example.com",
                Password = "password46",
                PhoneNumber = "0901234606",
                Address = "143 Sóc Trăng"
            },
            new {
                Name = "Nguyễn Thị Kim",
                Email = "nguyenthikim@example.com",
                Password = "password47",
                PhoneNumber = "0901234607",
                Address = "144 Bạc Liêu"
            },
            new {
                Name = "Trần Văn Đức",
                Email = "tranvanduc@example.com",
                Password = "password48",
                PhoneNumber = "0901234608",
                Address = "145 Cà Mau"
            },
            new {
                Name = "Lê Thị Thu",
                Email = "lethithu@example.com",
                Password = "password49",
                PhoneNumber = "0901234609",
                Address = "146 Đắk Nông"
            },
            new {
                Name = "Phan Văn Tài",
                Email = "phanvantai@example.com",
                Password = "password50",
                PhoneNumber = "0901234610",
                Address = "147 Lâm Đồng"
            },

           };

        //users = new[] {
        //    new {
        //        Name = "Trần Văn Tuấn",
        //        Email = "tranvantuan@example.com",
        //        Password = "password1",
        //        PhoneNumber = "0901234561",
        //        Address = "123 Ho Chi Minh City"
        //    }, };

        foreach (var user in users)
        {
            var newUser = new User
            {
                Name = user.Name,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Address = user.Address

            };
            newUser.UserName = newUser.Id;

            var result = await _userManager.CreateAsync(newUser, user.Password);
            Console.WriteLine(result);
        }

    }
}
