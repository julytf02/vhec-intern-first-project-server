﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Reflection.Metadata;
using vhec_intern_first_project_server.Models;

namespace vhec_intern_first_project_server.Data;


public class AppDbContext : IdentityDbContext<User>
{
    public DbSet<User> Users { get; set; }

    private readonly IConfiguration _configuration;

    public AppDbContext(DbContextOptions<AppDbContext> options, IConfiguration configuration)
       : base(options)
    {
        _configuration = configuration;
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("sql"));
        }
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<User>().Property(c => c.Name)
        .UseCollation("SQL_LATIN1_GENERAL_CP1_CI_AI ");
        modelBuilder.Entity<User>().Property(c => c.Address)
        .UseCollation("SQL_LATIN1_GENERAL_CP1_CI_AI ");
    }
    public override int SaveChanges()
    {
        AddTimestamps();
        return base.SaveChanges();
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        AddTimestamps();
        return base.SaveChangesAsync();
    }

    private void AddTimestamps()
    {
        var entries = ChangeTracker
            .Entries()
            .Where(e => e.Entity is HasTimestamps && (
                    e.State == EntityState.Added
                    || e.State == EntityState.Modified));

        foreach (var entityEntry in entries)
        {
            Console.WriteLine(DateTime.Now);
            ((HasTimestamps)entityEntry.Entity).UpdatedDate = DateTime.Now;

            if (entityEntry.State == EntityState.Added)
            {
                ((HasTimestamps)entityEntry.Entity).CreatedDate = DateTime.Now;
            }
        }

    }
}
