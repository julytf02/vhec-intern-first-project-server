﻿using System.ComponentModel.DataAnnotations;

namespace vhec_intern_first_project_server.Controllers.Auth.Models
{
    public class LoginModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(8)]
        [StringLength(50)]
        public string Password { get; set; }
    }
}
