﻿using System.ComponentModel.DataAnnotations;

namespace vhec_intern_first_project_server.Controllers.Auth.Models
{
    public class RegisterModel
    {
        //[Required(ErrorMessage = "Name is required")]
        //public string? Name { get; set; }

        [Required]
        [EmailAddress]
        public string? Email { get; set; }

        [Required]
        [MinLength(8)]
        [StringLength(50)]
        public string? Password { get; set; }

        //[Required(ErrorMessage = "PhoneNumber is required")]
        //public string? PhoneNumber { get; set; }

        //[Required(ErrorMessage = "Address is required")]
        //public string? Address { get; set; }
    }
}
