using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using vhec_intern_first_project_server.Controllers.Auth.Models;
using vhec_intern_first_project_server.Data;
using vhec_intern_first_project_server.Models;

namespace vhec_intern_first_project_server.Controllers.Auth
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly int tokenLiveTime = 60;

        private readonly ILogger<AuthController> _logger;
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDBContext;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        public AuthController(
            ILogger<AuthController> logger,
        IConfiguration configuration,
            AppDbContext appDBContext,
            UserManager<User> userManager,
            IMapper mapper
            )
        {
            _logger = logger;
            _configuration = configuration;
            _appDBContext = appDBContext;
            _userManager = userManager;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel loginModel)
        {
            var user = await _userManager.FindByEmailAsync(loginModel.Email);

            if (user == null)
            {
                return NotFound(new
                {
                    status = 404,
                    message = "Email Not Found!",
                });
            }

            if (!await _userManager.CheckPasswordAsync(user, loginModel.Password))
            {
                return Unauthorized(new
                {
                    status = 401,
                    message = "Wrong Password!",
                });
            }

            var token = await GetToken(user);

            var mappedUser = _mapper.Map<UserDto>(user);

            return Ok(new
            {
                status = 200,
                message = "Login Successful!",
                data = new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo,
                    user = mappedUser
                }
            });

        }
        [HttpPost]
        [Route("relogin")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> ReLogin()
        {
            var userName = User.Identity.Name;

            var user = await _userManager.FindByNameAsync(userName);

            if (user == null)
            {
                return NotFound(new
                {
                    status = 404,
                    message = "User Not Found!",
                });
            }

            var mappedUser = _mapper.Map<UserDto>(user);

            return Ok(new
            {
                status = 200,
                message = "Login Successful!",
                data = new
                {
                    user = mappedUser
                }
            });

        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel registerModel)
        {
            var userExists = await _userManager.FindByEmailAsync(registerModel.Email);
            if (userExists != null)
                return Conflict(new { status = 409, message = "User already exists!" });

            User user = new()
            {
                Email = registerModel.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            user.UserName = user.Id;

            var result = await _userManager.CreateAsync(user, registerModel.Password);
            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError, new { status = 500, message = "User creation failed! Please check user details and try again." });

            var token = await GetToken(user);

            var mappedUser = _mapper.Map<UserDto>(user);

            return Ok(new
            {
                status = 201,
                message = "User created successfully!",
                data = new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo,
                    user = mappedUser
                }
            });
        }

        // [HttpPost]
        // [Route("logout")]
        // public async Task<IActionResult> Logout()
        // {
        //     // cancel the token

        //     return Ok(new { status = 200, message = "Logout Successful!" });
        // }

        [HttpGet]
        [Route("check-email")]
        public async Task<IActionResult> Register([FromQuery] string email)
        {
            var userExists = await _userManager.FindByEmailAsync(email);
            if (userExists != null)
                return BadRequest(new { status = 400, message = "User already exists!" });

            return Ok(new { status = 200, message = "User not exists!" });
        }

        private async Task<JwtSecurityToken> GetToken(User user)
        {
            var userRoles = await _userManager.GetRolesAsync(user);

            var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

            foreach (var userRole in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, userRole));
            }

            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddHours(tokenLiveTime),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return token;
        }
    }
}