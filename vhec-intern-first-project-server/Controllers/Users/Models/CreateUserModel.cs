﻿using System.ComponentModel.DataAnnotations;

namespace vhec_intern_first_project_server.Controllers.Users.Models;

public class CreateUserModel
{
    [StringLength(255)]
    public string? Name { get; set; }

    [Required]
    [EmailAddress]
    public string Email { get; set; }
    
    [Required]
    [MinLength(8)]
    [StringLength(50)]
    public string Password { get; set; }

    [Phone]
    public string? PhoneNumber { get; set; }

    [StringLength(255)]
    public string? Address { get; set; }
}
