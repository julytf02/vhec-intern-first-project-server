﻿using System.ComponentModel.DataAnnotations;

namespace vhec_intern_first_project_server.Controllers.Users.Models;

public class QueryModel
{
    public int Page { get; set; } = 1;
    public int PerPage { get; set; } = 24;

    public string? SortBy { get; set; }
    public string Order { get; set; } = "asc";

    public string? Id { get; set; }
    public string? Name { get; set; }
    public string? Email { get; set; }
    public string? PhoneNumber { get; set; }
    public string? Address { get; set; }
}
