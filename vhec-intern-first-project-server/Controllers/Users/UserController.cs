
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using vhec_intern_first_project_server.Controllers.Users.Models;
using vhec_intern_first_project_server.Data;
using vhec_intern_first_project_server.Models;

namespace vhec_intern_first_project_server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class UserController : Controller
    {

        private readonly ILogger<UserController> _logger;
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _appDBContext;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public UserController(
            ILogger<UserController> logger,
            IConfiguration configuration,
            AppDbContext appDBContext,
            UserManager<User> userManager,
            IMapper mapper
            )
        {
            _logger = logger;
            _configuration = configuration;
            _appDBContext = appDBContext;
            _userManager = userManager;
            _mapper = mapper;
        }

        [HttpGet(Name = "GetUsers")]
        public IActionResult Get([FromQuery] QueryModel queryModel)
        {
            var query = _appDBContext.Users.AsQueryable();

            if (queryModel.Order == "asc")
            {
                switch (queryModel.SortBy)
                {
                    case "id":
                        query = query.OrderBy(u => u.Id);
                        break;
                    case "name":
                        query = query.OrderBy(u => u.Name);
                        break;
                    case "email":
                        query = query.OrderBy(u => u.Email);
                        break;
                    case "phoneNumber":
                        query = query.OrderBy(u => u.PhoneNumber);
                        break;
                    case "address":
                        query = query.OrderBy(u => u.Address);
                        break;
                }
            }
            else if (queryModel.Order == "desc")
            {
                switch (queryModel.SortBy)
                {
                    case "id":
                        query = query.OrderByDescending(u => u.Id);
                        break;
                    case "name":
                        query = query.OrderByDescending(u => u.Name);
                        break;
                    case "email":
                        query = query.OrderByDescending(u => u.Email);
                        break;
                    case "phoneNumber":
                        query = query.OrderByDescending(u => u.PhoneNumber);
                        break;
                    case "address":
                        query = query.OrderByDescending(u => u.Address);
                        break;
                }
            }

            if (!string.IsNullOrEmpty(queryModel.Id))
            {
                query = query.Where(u => u.Id.Contains(queryModel.Id));
            }
            if (!string.IsNullOrEmpty(queryModel.Name))
            {
                query = query.Where(u => u.Name.Contains(queryModel.Name));
            }
            if (!string.IsNullOrEmpty(queryModel.Email))
            {
                query = query.Where(u => u.Email.Contains(queryModel.Email));
            }
            if (!string.IsNullOrEmpty(queryModel.PhoneNumber))
            {
                query = query.Where(u => u.PhoneNumber.Contains(queryModel.PhoneNumber));
            }
            if (!string.IsNullOrEmpty(queryModel.Address))
            {
                query = query.Where(u => u.Address.Contains(queryModel.Address));
            }

            var totalItems = query.Count();

            //TODO: validate model
            var skip = Math.Max(queryModel.PerPage * (queryModel.Page - 1), 0);
            var perPage = Math.Max(queryModel.PerPage, 1);

            query = query.Skip(skip).Take(perPage);

            var users = query.ToList();

            var mappedUsers = _mapper.Map<IEnumerable<UserDto>>(users);

            return Ok(new
            {
                status = 200,
                message = "Success",
                data = mappedUsers,
                pagination = new
                {
                    page = queryModel.Page,
                    perPage = queryModel.PerPage,
                    totalItems = totalItems,
                    totalPages = (int)Math.Ceiling((double)totalItems / queryModel.PerPage),
                }
            });
        }

        [HttpPost(Name = "CreateUser")]
        public async Task<IActionResult> Create([FromBody] CreateUserModel createUserModel)
        {
            var newUser = new User
            {
                Name = createUserModel.Name,
                Email = createUserModel.Email,
                PhoneNumber = createUserModel.PhoneNumber,
                Address = createUserModel.Address,
            };
            newUser.UserName = newUser.Id;

            var result = await _userManager.CreateAsync(newUser, createUserModel.Password);

            if (!result.Succeeded)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    status = 500,
                    message = "Failed to create user!",
                });
            }

            return Ok(new
            {
                status = 200,
                message = "Success"
            });
        }

        [HttpPut("{id}", Name = "UpdateUser")]
        public async Task<IActionResult> Update(string id, [FromBody] UpdateUserModel updateUserModel)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user == null)
            {
                return NotFound(new
                {
                    status = 404,
                    message = "User Not Found!",
                });
            }

            if (!string.IsNullOrEmpty(updateUserModel.Password))
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var changePasswordResult = await _userManager.ResetPasswordAsync(user, token, updateUserModel.Password);
                if (!changePasswordResult.Succeeded)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new
                    {
                        status = 500,
                        message = "Failed to update user!",
                        forDebug="pass"
                    });
                }
            }

            if (!string.IsNullOrEmpty(updateUserModel.PhoneNumber))
            {
                // var token = await _userManager.GenerateChangePhoneNumberTokenAsync(user, updateUserModel.PhoneNumber);
                var changePhoneNumberResult = await _userManager.SetPhoneNumberAsync(user, updateUserModel.PhoneNumber);
                if (!changePhoneNumberResult.Succeeded)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new
                    {
                        status = 500,
                        message = "Failed to update user!",
                        forDebug = "phone"
                    });
                }
            }

            if (!string.IsNullOrEmpty(updateUserModel.Email))
            {
                // var token = await _userManager.GenerateChangeEmailTokenAsync(user, updateUserModel.Email);
                var changeEmailResult = await _userManager.SetEmailAsync(user, updateUserModel.Email);
                if (!changeEmailResult.Succeeded)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new
                    {
                        status = 500,
                        message = "Failed to update user!",
                        forDebug = "email"
                    });
                }
            }

            user.Name = updateUserModel.Name;
            user.Address = updateUserModel.Address;

            var updateResult = await _userManager.UpdateAsync(user);

            if (!updateResult.Succeeded)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    status = 500,
                    message = "Failed to update user!",
                    forDebug = "general"
                });
            }

            return Ok(new
            {
                status = 200,
                message = "Success"
            });
        }


        [HttpDelete("{id}", Name = "DeleteUser")]
        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user == null)
            {
                return NotFound(new
                {
                    status = 404,
                    message = "User Not Found!",
                });
            }

            var deleteResult = await _userManager.DeleteAsync(user);

            if (!deleteResult.Succeeded)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    status = 500,
                    message = "Failed to delete user!",
                });
            }

            return Ok(new
            {
                status = 200,
                message = "Success"
            });
        }

    }
}