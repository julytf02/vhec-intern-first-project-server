﻿using AutoMapper;
using vhec_intern_first_project_server.Models;

namespace vhec_intern_first_project_server.Mappings;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<User, UserDto>();
    }
}
