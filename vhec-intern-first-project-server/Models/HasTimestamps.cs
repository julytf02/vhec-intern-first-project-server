using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace vhec_intern_first_project_server.Models
{
    public interface HasTimestamps
    {
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

    }
}