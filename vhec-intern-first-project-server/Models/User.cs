using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace vhec_intern_first_project_server.Models
{
    public class User : IdentityUser, HasTimestamps
    {
        //[Key]
        //public string ID { get; set; }
        public string? Name { get; set; }
        //public string Email { get; set; }
        //public string PasswordHash { get; set; }
        //public string PhoneNumber { get; set; }
        public string? Address { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

    }
}